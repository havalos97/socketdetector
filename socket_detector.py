from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
import argparse
import imutils
import numpy as np
import cv2
import time
import os

cap = cv2.VideoCapture(0)
width = 0.1

def midpoint(ptA, ptB):
	return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)

boundaries = [
	([103, 86, 65], [145, 133, 128])
]

def applymask(image, low, hi):
	lower = np.array([103, 86, 65], dtype = "uint8")
	upper = np.array([145, 133, 128], dtype = "uint8")
	mask = cv2.inRange(image, lower, upper)
	return cv2.bitwise_and(image, image, mask=mask)
	pass

while(True):
	try:
		ret, frame = cap.read() # CAPTURE FRAME-BY-FRAME

		if((frame is not None) and (ret is not None)): # MAKING SURE FRAME COMING FROM CAMERA IS NOT NONE
			image = frame
			original = image.copy()
			height, width, channels = image.shape
			image = frame[(height/2)-15:((height/2)+15), (width/4):((width/4)+320)]

			output = applymask(image, [103, 86, 65], [145, 133, 128])
			grayscale = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
			nonzero = cv2.countNonZero(grayscale)
			print(nonzero)
			# show the images
			cv2.imshow("Mask", image)
			cv2.imshow("Grayscale", grayscale)
			os.system("clear")
			if (nonzero >= 0 and nonzero < 10):
				print("Empty socket feeder")
			elif (nonzero > 20 and nonzero < 1000):
				print("Inverted socket")
				coincidence = image.copy()
				cv2.imshow("Coincidence", coincidence)
			elif (nonzero > 1150):
				print("Right positioned socket")

		cv2.imshow("Orig", original)
		if(cv2.waitKey(1) & 0xFF == ord("q")):
			break # BREAKS INFINITE LOOP WHEN CTRL+Q IS PRESSED
	except:
		raise

# WHEN EVERYTHING DONE, RELEASE THE CAPTURE
cap.release()
cv2.destroyAllWindows()
